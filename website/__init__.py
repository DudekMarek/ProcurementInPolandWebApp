from flask import Flask
import os
import logging
from prometheus_client import start_http_server, Gauge, Summary, Counter

# Configuration of environment variables
APP_NAME = os.environ.get('WSB_APP_NAME', 'wsb-app')
LOG_LEVEL = os.environ.get('WSB_APP_LOG_LEVEL', "")
APP_SECRET_KEY = os.environ.get('WSB_APP_SECRET_KEY', "super_extra_secret_key")

# Setting the logging level
if LOG_LEVEL.upper() == 'DEBUG':
    log_level = logging.DEBUG
elif LOG_LEVEL.upper() == 'ERROR':
    log_level = logging.ERROR
else:
    log_level = logging.INFO

# Configuration of log format and date format
log_format = '%(asctime)s %(levelname)s : : %(message)s'
date_format = '%Y-%m-%d %H:%M:%S'

# Configuration of metrics
VISITS_COUNTER = Counter('page_visits_count', 'Number of page visits', ['method', 'endpoint'])
API_CALLS_COUNTER = Counter('api_calls_count', 'Number of API calls', ['endpoint'])
API_CALL_TIME_LAST = Gauge('api_call_processing_time_last', 'Time spent processing last API call', ['endpoint'])
API_CALL_TIME_SUM = Summary('api_call_processing_time_summary', 'Time spent processing API calls', ['endpoint'])


def create_app():
    app = Flask(__name__)
    
    logging.basicConfig(level=log_level, format=log_format, datefmt=date_format)
    

    from .views import views
    app.register_blueprint(views)
    app.secret_key = APP_SECRET_KEY
    
    return app