import locale
import time
import requests
from flask import Blueprint, render_template, request, current_app, flash
from . import VISITS_COUNTER, API_CALLS_COUNTER, API_CALL_TIME_LAST, API_CALL_TIME_SUM

views = Blueprint("views", __name__)

# Displaying the homapage
@views.route('/')
def home():
    VISITS_COUNTER.labels(method="GET", endpoint="/").inc()
    return render_template('home.html')

# Displaying API data
@views.route('/dane-o-zamowieniach', methods=['GET', 'POST'])
def procurments():
    VISITS_COUNTER.labels(method="GET", endpoint="/dane-o-zamowieniach").inc()
    url = 'https://tenders.guru/api/pl/tenders'
    
    # Handling navigation to the next pages
    if request.method == 'POST':
        page = request.form.get('page')
        params = {"page": f"{page}"}
    # Handling navigation to first page
    else:
        params = "1"
    
    
    
    start_time = time.monotonic()
    allData = requests.get(url, params=params)
    end_time = time.monotonic()
    
    if(allData.status_code != 200):
        flash('Błąd w odpowiedzi od API', category='error')
        current_app.logger.error(f"Error in the API response for page number {page}. Response code: {allData.status_code}")
        
        return render_template('procurment.html')
            
    else:
    
        allData = allData.json()
        response_time = end_time - start_time
        API_CALL_TIME_SUM.labels(endpoint=url).observe(response_time)
        API_CALL_TIME_LAST.labels(endpoint=url).set(response_time)
        API_CALLS_COUNTER.labels(endpoint=url).inc()
        
        currentPage = allData['page_number']
            
        # Extracting from the entire JSON file a list of objects describing individual orders
        data = allData['data']
        for proc in data:
            proc['awarded_value_eur'] = format_number(proc['awarded_value_eur'])
        
            
        return render_template('procurment.html', data=data, currentPage=currentPage)

# The function used for formatting numbers to make them easier to read
def format_number(num_str):
    
    if '.' in num_str:
        integer_part, decimal_part = num_str.split('.')
    else:
        integer_part = num_str
        decimal_part = "00"
    
    
    integer_part = integer_part[::-1]
    integer_part = " ".join([integer_part[i:i+3] for i in range(0, len(integer_part), 3)])
    integer_part = integer_part[::-1]
    
    return f"{integer_part},{decimal_part}"




# Displaying order details
@views.route('/details/<int:id>')
def details(id):
    API_CALLS_COUNTER.labels(endpoint="https://tenders.guru/api/pl/tenders/id").inc()
    VISITS_COUNTER.labels(method="GET", endpoint="/details/").inc()

    current_app.logger.info(f"Processing details for tender ID: {id}")
    
    url = f'https://tenders.guru/api/pl/tenders/{id}'
    
    start_time = time.monotonic()
    data = requests.get(url).json()
    end_time = time.monotonic()
    
    response_time = end_time - start_time
    
    API_CALL_TIME_SUM.labels(endpoint="https://tenders.guru/api/pl/tenders/id").observe(response_time)
    API_CALL_TIME_LAST.labels(endpoint="https://tenders.guru/api/pl/tenders/id").set(response_time)
    
    data['awarded_value'] = format_number(data['awarded_value'])
    data['awarded_value_eur'] = format_number(data['awarded_value_eur'])
    
    return render_template('details.html', data=data)

# Health check endpoint
@views.route('/health-check-endpoint')
def health_check():
    VISITS_COUNTER.labels(method="GET", endpoint="/health-check-endpoint").inc()
    return "It works"