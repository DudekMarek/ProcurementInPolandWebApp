from website import create_app
from prometheus_client import start_http_server
app = create_app()

start_http_server(9090, addr='0.0.0.0')

if __name__ == '__main__':
    app.run(port=80 ,host='0.0.0.0')