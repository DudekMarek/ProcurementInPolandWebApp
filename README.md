# ProcurementInPolandWebApp
---
### Aplikacja powstała na potrzeby warsztatów "Automating the Future: DevOps in Practice"
---
## 1. Działanie aplikacji
- Aplikacja składa się ze strony głównej (wyświetlającej komunikat) oraz strony wyświetlającej dane o zamówieniach 
- Po wejściu na stronę __,,/dane-o-zamowieniach"__ pokazuje się pierwsza strona zamówień (pobrana z zewnętrznego [API](https://tenders.guru/pl/api))
- Aby przeglądać kolejne strony należy przejść do końca strony i w polu __,,Numer strony''__ wpisać konkretny numer. A następnie kliknąc przycisk __,,Idź''__
- Podczas przeglądania strony __,,/dane-o-zamowieniach''__ można kliknąć w każdy pogrubiony ID zamówienia. Taka czynność przeniesie użytkownika do strony wyświetlającej dokładne informacje o danym zamówieniu
- Aplikacja posiada endpoint __,,/health-check-endpoint''__ do którego nie ma bezpośredniego linka na stronie służy on jedynie do sprawdzania stanu przez __AWS Load Balancer__
- Aplikacja zbiera metryki za pomocą __prometheus_client__ i wystawia je na porcie 9090. Metryki te następnie są zbierane przez agenta Telegraf i wysyłane do Grafana Cloud

---

## 2. Omówienie działania CICD pipeline

Cały pipeline składa się z 2 __stage__ :  __test__ i __publish__
 - __test__ ma za zadnaie przetestować aplikację 
    - aby to zrobić korzysta z oficjalnego obrazu Python (__python:3.10.8-alpine3.16__)
    - po wczytaniu obrazu instalowane są zależności z pliku _requirements.txt_
    - następnie instalowane są biblioteki potrzebne jedynie podczas testowania aplikacji (_pytest_ i _responses_)
    - ostatnim etapem jest właściwe przetestowanie aplikacji

- __publish__ ma za zadanie zbudować obraz a następnie umieścić go w zdalnym repozytorium __AWS ECR__ 
    - aby to zrobić korzysta z oficjalnego obrazu __amazon/aws-cli__ (obraz ten posiada preinstalowane __AWS CLI__ które jest potrzebne do logowania do AWS)
    - obraz __amazon/aws-cli__ nie posiada zainstalowanego _Dockera_ dlatego najpierw jest on instalowany (po zainstalowaniu drukowane są do konsoli wersja _AWS CLI_ oraz wersja _Docker_)
    - następnie budowany jest obraz aplikacji który jako tag używa zmiennej środowiskowej __$CI_COMMIT_SHORT_SHA__ która reprezentuje skrócony identyfikator dla bierzącego commita
    - po udanym zbudowaniu obrazu _Dockera_ następuje logowanie do __AWS ECR__. _acces key_ i _secret acces key_ są przechowywane jako zmienne środowiskowe w GitLab 
    - na koniec obraz wypychany jest do __AWS ECR__

---

## 3. Budowa infrastruktury AWS

Do budowy infrastruktury AWS użyto __Terraform__ cały kod terraform (wraz z omówieniem infrastruktury) znajduje się w tym [repozytorium](https://gitlab.com/DudekMarek/infrastructureforprocurementwebapp)

---

## 4. Zbieranie metryk i logów.

### 4.1 Metryki

- do zbierania metryk użyto agenta __Telegraf__ 
- metryki zbierane przez agenta wysyłąne są do __Prometheus__ umieszczonego na __Grafana Cloud__ 
-  agent __Telegraf__ jest uruchamiany jako osobny _Task_ w _AWS ECS_ 

### 4.2 Logi

- aplikacja standardowo loguje na poziomie __INFO__ można to zmienić za pomocą zmiennej środowiskowej __LOG_LEVEL__ dostępne opcje to __DEBUG__ oraz __ERROR__
- logi zbierane są przez agenta __Fluent Bit__ i wysyłane do __Loki__ zainstalowanego na __Grafana Cloud__

---

## 5. Wizualizacja metryk i logów

### Do zwizualizowania logów i metryk użyto __Grafana__ gdzie utworzono _Dashboard_ na którym umieszczono:

- logi

![logi](photos/logi.PNG)

- liczbę odwiedzin strony /dane-o-zamowieniach w każdej minucie

![liczba odwiedzin](photos/liczba_odwiedzin.PNG)

- liczbę odwiedzin strony /details w każdej minucie

![liczba odwiedzin](photos/liczba_odwiedzin1.PNG)

- liczbę odwiedzin strony / w każdej minucie

![liczba odwiedzin](photos/liczba_odwiedzin2.PNG)

- sumaryczne liczby odwiedzin 

![sumaryczna liczba odwiedzin](photos/liczba_odwiedzin_sum.PNG)

- ostatni czas odpowiedzi od API

![cas odpowiedzi od API](photos/ostatni_czas.PNG)

- historyczne czasy odpowiedzi od API

![cas odpowiedzi od API](photos/czas_odpowiedzi.PNG)
 
- liczba zapytań do endpoint /health-check-endpoint

![liczba zapytań - /health-check-endpoint](photos/liczba_zapytan.PNG)